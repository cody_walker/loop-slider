slider = class("slider")
slider_playback = require("slider.slider_playback")
slider_collision = require("slider.slider_collision") 
slider_draw = require("slider.slider_draw")
slider_marker = require("slider.slider_marker")

-- definitions
clock = 0
mouse = Vector(love.mouse.getPosition()) 
marker_sticky_radius = 50


function slider:_init(x, y, size, max_markers, paused, fps)
    self.playback_index = 1
    self.position = Vector(x, y)
    self.size = size 
    self.h = 50
    
    self.mouse_index = false 
    self.max_markers = max_markers or 2
    self.markers = {}
    self.hitbox_markers = {}
    self.selected = false
    self.dragged = false
    self.loops = {}    
    local first_loop = {loop_start = 0, loop_end = size}
    table.insert(self.loops, first_loop)

    local fps = fps or 30
    self.seconds_per_frame = 1/fps
    
    -- state
    self.playing = true
    self.paused = paused or false
    self.active = true
end

-- input 
function slider:mousepressed(x, y, button)
    if self.active == false then return end
    -- left click
    if button == "l" then
        -- check for active marker
        local is_over_a_marker = false
        if #self.hitbox_markers > 0 then
            is_over_a_marker = true
        end
        
        if self.max_markers == 1 then 
            if #self.markers < self.max_markers then
                slider_marker.create(self, x)
            elseif #self.markers == 1 then
                self.markers[1].x = x 
            end
            return
        end

        if #self.markers < self.max_markers  then
            slider_playback.loop_create(self, x)
        elseif is_over_a_marker then 
            local closest = slider_marker.find_closest(self, x) 
            -- make sure it exists
            if self.markers[closest] then
                local closest_marker = self.markers[closest]
                self.dragged = closest
                self.selected = closest_marker.tethered 
            end
        end 
    end
end

function slider:mousereleased(x, y, button)
    -- create a loop
    if button == "l" then
        self.selected = false
        self.dragged = false
    end
end 

local function increment_loop(self, value)
    -- need something selected to increment
    if not self.selected or not self.dragged then return end

    if love.keyboard.isDown("lshift") then
        self.playback_index = self.markers[self.selected].x + value
        slider_marker.update(self, self.selected, self.markers[self.selected].x+self.position.x + value) 
    else
        -- pause so the last frame is visible
        self.paused = true
        self.playback_index = self.markers[self.dragged].x + value
        slider_marker.update(self, self.dragged, self.markers[self.dragged].x+self.position.x + value) 
    end 
    slider_playback.update_loop(self) 
end

function slider:keypressed(key)
    if key == " " then
        self.paused = not self.paused
    elseif key == "left" then
        increment_loop(self, -1)
    elseif key == "right" then
        increment_loop(self, 1)
    end
end


-- update
function slider:update(dt)
    if self.active == false then return end

    mouse = Vector(love.mouse.getPosition())
    down = love.mouse.isDown("l")
    if down then
        -- drag the marker
        if self.dragged then
            slider_marker.update(self, self.dragged, mouse.x) 
            slider_playback.update_loop(self) 
        end
        -- hide the cursor
        self.mouse_index = false 
    end 
    
    -- mouseover
    slider_collision.check_mouseover(self)
    slider_collision.check_mouseover_hitboxes(self) 
    slider_collision.check_mouseover_loops(self)
    
    -- playback
    if self.playing and not self.paused then
        slider_playback.update_playback(self, dt)
    end 
    
    -- lshift to scrub to beginning of playback
    if love.keyboard.isDown("lshift") then
        -- self.playing = false
        self.mouse_index = false
        self.playback_index = self.loops[#self.loops].loop_start
    end 
    
end


function slider:draw() 
    if self.active == false then return end

    slider_draw.background(self)
    slider_draw.loop_regions(self)
    
    for i=1, #self.markers do
        slider_draw.marker(self, i) 
    end 
    
    if self.max_markers == 2 then
        slider_draw.playback_index(self)
    end
    
    if self.mouse_index then
        slider_draw.mouse_index(self)
    end 
end 

return slider



