local help = {}

-- clamps a number to within a certain range
function help.clamp(low, n, high) return math.min(math.max(n, low), high) end

-- finds the modulo of a number within a range that starts with 1
function help.mod_list(index, length) return (index - 1) % length + 1 end

--- returns hex representation of num
function help.num_to_hex(num)
    local hex_str = '0123456789abcdef'
    local s = ''
    while num > 0 do
        local mod = math.fmod(num, 16)
        s = string.sub(hex_str, mod+1, mod+1) .. s
        num = math.floor(num / 16)
    end
    if s == '' then s = '0' end
    return s
end

-- random real number in [0, 1]
function help.random()
    return love.math.random()
end

-- random real number in [min, max]
function help.random_num(min, max)
    return love.math.random() + help.random_int(min, max - 1)
end

-- random integer in [min, max]
function help.random_int(min, max)
    return love.math.random(min, max)
end

-- random boolean value
function help.random_bool()
    return love.math.random(0, 1) == 1
end

-- shuffle the order of the elements inside a table
function help.shuffle(t)
    local n = #t

    while n >= 2 do
        -- n is now the last pertinent index
        local k = love.math.random(n) -- 1 <= k <= n
        -- Quick swap
        t[n], t[k] = t[k], t[n]
        n = n - 1
    end

    return t
end

-- re-maps a number from one range to another
function help.map(value, old_min, old_max, new_min, new_max)
    assert(value >= old_min and value <= old_max, 'value must be inside original range')

    local old_range = old_max - old_min
    local new_range = new_max - new_min
    return (((value - old_min) * new_range) / old_range) + new_min
end

-- invert a table so that each value is the key holding one key to that value 
-- in the original table.
function help.invert(t)
    local i={}
    for k,v in pairs(t) do 
        i[v] = k
    end
    return i
end


function help.split_file_name(strfilename)
	-- Returns the Path, Filename, and Extension as 3 values
	return string.match(strfilename, "(.-)([^\\]-([^\\%.]+))$")
end
-- local path,file_name,extension = help.split_file_name(file)

return help
