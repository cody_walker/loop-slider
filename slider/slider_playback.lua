local slider_playback = {}

function slider_playback.loop_create(slider, x) 
    slider_marker.create(slider, x) 
    slider.selected = #slider.markers 
    slider_marker.create(slider, x) 
    slider.dragged = #slider.markers 
    
    table.insert(slider.loops, {})
    slider.markers[#slider.markers-1].loop_index = #slider.loops
    slider.markers[#slider.markers].loop_index = #slider.loops 
    slider.markers[#slider.markers-1].tethered = #slider.markers
    slider.markers[#slider.markers].tethered = #slider.markers-1
end

function slider_playback.loop_destroy(slider)
    local loop_to_destroy = slider_collision.check_mouseover_loops(slider)
    if loop_to_destroy then
        local loop_index_to_destroy
        for i=2, #slider.loops do
            slider_marker.destroy_loop_markers(slider, i) 
            if slider.loops[i] == loop_to_destroy then
                table.remove(slider.loops, i) 
            end 
        end
    end 
end

function slider_playback.update_loop(slider)
    if not slider.dragged or not slider.selected then return end

    
    local point_1 = slider.markers[slider.selected].x
    local point_2 = slider.markers[slider.dragged].x
    
    -- make the earliest point the beginning of the loop
    if point_1 > point_2 then
        point_1, point_2 = point_2, point_1
    end
    
    local loop = {loop_start = point_1, loop_end = point_2}
    loop_index = slider.markers[slider.dragged].loop_index
    slider.loops[loop_index] = loop 
end

-- advance the index
function slider_playback.advance_index(slider, increment)
    local increment = increment or 1
    local current_loop = slider.loops[#slider.loops]
    local loop_start = current_loop.loop_start
    local loop_end = current_loop.loop_end
    
    -- adjust index by the loop_start offset
    local loop_start_adjust = loop_start
    
    -- if over the offset, set it to the start of the loop
    if loop_start > slider.playback_index then loop_start_adjust = slider.playback_index end
    
    local loop_index_length = loop_end - loop_start 
    local index_in_loop = (slider.playback_index + increment - loop_start_adjust) % loop_index_length
    slider.playback_index = index_in_loop + loop_start
end

function slider_playback.update_playback(slider, dt)
    clock = clock + dt
    if clock > slider.seconds_per_frame then
        slider_playback.advance_index(slider)
        clock = clock % slider.seconds_per_frame 
    end 
end

return slider_playback

