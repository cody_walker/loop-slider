filesystem = require ("love.filesystem")
local channel = love.thread.getChannel("request_channel")

while true do
    local message = channel:demand ()
    local name = message.name
    local save_folder_name = message.save_folder
    local raw_folder_name = message.raw_folder
    local filename = message.filename
    local start_time = message.start_time
    local duration = message.duration
    local width = message.width
    local height = message.height
    local resolution = width .. "x" .. height
    local fps = message.fps
    local format = message.format
    local start_time_readable = tostring(duration):gsub("%..*", "") 
    

    if format == "png" then
        cmd = ("ffmpeg -ss " .. start_time ..
                   " -t " .. duration ..
                   " -i \"" .. filename ..
                   "\" -s:v " .. resolution ..
                   " -r " .. fps ..
                   " " .. raw_folder_name .. "/%04d.png")
        os.execute(cmd) 
    elseif format == "gif" then
        local palette_cmd = "ffmpeg -y -ss " .. start_time ..
            " -t " .. duration ..
            " -i " .. filename ..
            " -vf fps=" .. fps ..
            ",scale=" .. height ..
            ":-1:flags=lanczos,palettegen " .. save_folder_name .. "/" .. "palette.png"
        os.execute(palette_cmd)


        local filters = "'fps=" .. fps .. ",scale=" .. height .. ":-1:flags=lanczos[x];[x][1:v] paletteuse'"
        cmd = "ffmpeg -ss " .. start_time ..
            " -t " .. duration ..
            " -i " .. filename ..
            " -i " .. save_folder_name .. "/" .. "palette.png " .. " -filter_complex " .. filters .. " -y " ..
            save_folder_name .. "/" .. start_time_readable .. ".gif"
 
        os.execute(cmd) 
    end 
end
