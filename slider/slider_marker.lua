local slider_marker = {}

function slider_marker.create(slider, x)
    local marker = {}
    marker.x = x - slider.position.x
    marker.hitbox_top_left = Vector(x-marker_sticky_radius, slider.position.y)
    marker.hitbox_bottom_right = Vector(x+marker_sticky_radius, slider.position.y + slider.h)
    table.insert(slider.markers, marker)
end 

function slider_marker.destroy_hitbox_markers(slider)
    -- iterate backwards over table
    for i=#slider.hitbox_markers, 1, -1 do
        local slider_to_destroy = slider.hitbox_markers[i]
        table.remove(slider.markers, slider_to_destroy)
    end
end

function slider_marker.destroy_loop_markers(slider, loop_index)
    -- iterate backwards over table
    for i=#slider.markers, 1, -1 do
        if slider.markers[i].loop_index == loop_index then
            table.remove(slider.markers, i)
        end
    end
end


function slider_marker.find_closest(slider, x)
    local closest_marker = false
    local closest_marker_distance = slider.size 
    
    if #slider.markers == 0 then return false end
    if #slider.markers == 1 then
        if x > slider.size/2 then
            return 1, slider.size
        else
            return 1, 0
        end 
    end
    
    for i=1, #slider.markers do
        local marker_distance = x - slider.markers[i].x - slider.position.x
        -- local marker_distance = slider.markers[i].x - x
        if marker_distance ~= 0 then 
            -- don't check against itself
            if math.abs(marker_distance) < math.abs(closest_marker_distance) then
                closest_marker_distance = marker_distance
                closest_marker = i
            end
        end 
    end 
    -- return the marker index and the location
    return closest_marker, closest_marker_distance
end


function slider_marker.update(slider, index, x)
    local marker = slider.markers[index]
    
    -- floor so that we keep it in line with the playback index
    marker.x = math.floor(x - slider.position.x)
    -- limit x position to the edge of the marker
    marker.x = help.clamp(0, marker.x, slider.size)
    marker.hitbox_top_left = Vector(marker.x-marker_sticky_radius+slider.position.x, slider.position.y)
    marker.hitbox_bottom_right = Vector(marker.x+marker_sticky_radius+slider.position.x, slider.position.y + slider.h) 
end

return slider_marker

