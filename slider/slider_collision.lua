local slider_collision = {}
-- collision
function check_mouse_collision(top_left, bottom_right)
    local x1, y1 = top_left.x, top_left.y
    local x2, y2 = bottom_right.x, bottom_right.y 
    return mouse.x > x1 and mouse.x < x2 and mouse.y > y1 and mouse.y < y2
end

function slider_collision.check_mouseover_hitboxes(slider)
    slider.hitbox_markers = {}
    for i=1, #slider.markers do 
        if check_mouse_collision(slider.markers[i].hitbox_top_left, slider.markers[i].hitbox_bottom_right) then
            -- add to a table of markers in range of hitbox
            table.insert(slider.hitbox_markers, i)
        end
    end 
end

function slider_collision.check_mouseover_loops(slider)
    -- don't touch the first loop
    for i=2, #slider.loops do 
        loop_top_left = Vector(slider.loops[i].loop_start+slider.position.x, slider.position.y)
        loop_bottom_right = Vector(slider.loops[i].loop_end+slider.position.x, slider.position.y+slider.h)
        if check_mouse_collision(loop_top_left, loop_bottom_right) then
            return slider.loops[i]
        end
    end 
    return false
end 

function slider_collision.check_mouseover(slider) 
    -- check slider collision
    local slider_botright = Vector(slider.position.x+slider.size, slider.position.y+slider.h)
    if check_mouse_collision (slider.position, slider_botright) then
        -- mouse inside slider
        slider.playing = false
        slider.mouse_index = mouse.x - slider.position.x 
    else 
        -- mouse outside slider
        slider.playing = true
        slider.mouse_index = false
    end 
end

return slider_collision
