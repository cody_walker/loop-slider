local Color = require('util.Color')
local graphics = {}

function graphics.draw_rectangle(rect)
    -- create a rectangle object if missing
    if not rect then rect = {} end

    -- use the objects values unless
    local mode = rect.mode or 'fill' -- line or fill
    local x = rect.x or 0
    local y = rect.y or 0
    local w = rect.w or 10
    local h = rect.h or 10

    -- set a color if provided
    if rect.color then Color.set(rect.color) end

    -- draw rectangle
    love.graphics.rectangle(mode, x, y, w, h)
end

function graphics.draw_circle(circ)

    -- create a circle object if missing
    if not circ then circ = {} end

    local mode = circ.mode or 'fill'
    local x = circ.x or 0
    local y = circ.y or 0
    local radius = circ.radius or circ.r or 10
    local segments = circ.segments or circ.s or 10

    -- set a color if provided
    if circ.color then Color.set(circ.color) end

    love.graphics.circle(mode, x, y, radius, segments)
end

function graphics.draw_sprite(sprite)
    if not sprite then sprite = {} end

    local image = sprite.image          -- a drawable object
    local x = sprite.x or 0             -- the position to draw the object (x-axis)
    local y = sprite.y or 0             -- the position to draw the object (y-axis)
    local r = sprite.r or 0             -- orientation (radians)
    local sx = sprite.sx or 1           -- scale factor (x-axis)
    local sy = sprite.sy or 1           -- scale factor (y-axis)
    local ox = sprite.ox or 0           -- origin offset (x-axis)
    local oy = sprite.oy or 0           -- origin offset (y-axis)
    local kx = sprite.kx or 0           -- shearing factor (x-axis)
    local ky = sprite.ky or 0           -- shearing factor (y-axis)

     -- set a color if provided
    if sprite.color then Color.set(sprite.color) end

    love.graphics.draw(image, x, y, r, sx, sy, ox, oy, kx, ky)
end

function graphics.draw_graph(x, y, color, ratio, name, outline_color)
    local graph_bar =
    {
        color = color,
        x = x,
        y = y,
        w = 10,
        h = -40 * ratio,
        mode = 'fill'
    }
    graphics.draw_rectangle(graph_bar)

    -- draw outline
    local outline_bar =
    {
            color = outline_color or color,
            x = x,
            y = y,
            w = 10,
            h = -40,
            mode = 'line'
    }
    graphics.draw_rectangle(outline_bar)

    if name then
        love.graphics.print(name, x - 10, y + 5 , 0, 0.8)
    end
end

return graphics
