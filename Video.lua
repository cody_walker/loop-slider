Video = class("Video")

local function find_dimensions ()
    local dimensions_cmd = "ffprobe -v error -of flat=s=_ -select_streams v:0 -show_entries stream=height,width " .. folder .. "/".. files[1]
    local handle = io.popen(dimensions_cmd)
    local width = handle:read("*l")
    local height = handle:read("*l")
    handle:close()

    width = string.sub(width, 24, -1)
    height = string.sub(height, 25, -1) 
    return tonumber(width), tonumber(height)
end

local function find_duration()
    local duration_cmd = "ffprobe -i " .. folder .. "/".. files[1] .. " -show_entries format=duration -v quiet -of csv=\"p=0\""
    local handle = io.popen(duration_cmd)
    local duration = handle:read("*a")
    handle:close()
    duration = duration:gsub("%..*", "") 
    return tonumber(duration)
end

local function find_fps()
    local fps_cmd = "ffprobe -i " .. folder .. "/".. files[1] .. " -show_entries stream=avg_frame_rate -v quiet -of csv=\"p=0\""

    local handle = io.popen(fps_cmd)
    local fps = handle:read("*l")
    handle:close()

    fps = loadstring("return " .. fps)
    fps = fps()

    return tonumber(fps)
end


function Video:_init()

    self.width, self.height = find_dimensions()
    self.duration = find_duration()
    self.fps = find_fps()

end

function Video:refresh()
    self.width, self.height = find_dimensions()
    self.duration = find_duration() 
    self.fps = find_fps()

end


return Video
