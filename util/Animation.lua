local class = require 'util.class'
local Vector = require 'util.Vector'

local Animation = class('Animation')

function Animation:_init(obj)
    self.image_path = obj.image_path
    self.frame_index = 1
    self.frame_width = obj.frame_width
    self.frame_height = obj.frame_height
    self.playback_time = 0
    self.playback_speed = obj.playback_speed
    self.num_frames = obj.num_frames
    self.scale = obj.scale
    self.is_playing = true
    self.idle_frame = obj.idle_frame
    self.complete_callback = obj.complete_callback 
    self.image = love.graphics.newImage(self.image_path)
    self.image:setFilter('nearest', 'nearest')
    self.quad = love.graphics.newQuad(0, 0, self.frame_width, self.frame_height, self.image:getWidth(), self.image:getHeight())
    self.flip_horizontal = obj.flip_horizontal or 1
    self.flip_vertical = obj.flip_vertical or 1

    self.is_playing_once = false
end

function Animation:play()
    self.is_playing = true
end

function Animation:play_once()
    self.is_playing = true
    self.is_playing_once = true
end

function Animation:stop()
    self.is_playing = false
    self.is_playing_once = false
    self:go_to_frame(self.idle_frame)
end

function Animation:go_to_frame(index)
    self.frame_index = index % self.num_frames
    self.playback_time = self.frame_index / self.num_frames
end

function Animation:update(dt)
    -- play animation
    if self.is_playing then
        self.playback_time = self.playback_time + dt * self.playback_speed

        if self.playback_time > 1 then
            self.playback_time = self.playback_time % 1

            if self.play_once then
                self:stop()
            end

            -- trigger a callback on anim complete
            if self.complete_callback then
                self.complete_callback()
            end
        end
        self.frame_index = math.floor(self.playback_time * self.num_frames)
    end
end

function Animation:draw()

    love.graphics.push()
    love.graphics.scale(self.flip_horizontal, self.flip_vertical)
    love.graphics.translate(-self.frame_width/2, -self.frame_height/2)
    local frame_x = self.frame_index * self.frame_width
    local frame_y = 0

    self.quad:setViewport(frame_x, frame_y, self.frame_width, self.frame_height)

    love.graphics.draw( self.image, self.quad, 0, 0, 0, self.scale, self.scale)

    love.graphics.pop()
end

return Animation
