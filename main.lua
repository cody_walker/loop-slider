class = require "class"
help = require "util.help"
Color = require "util.Color" 
graphics = require "util.graphics"
Vector = require "util.Vector"

-- classes
slider = require "slider.slider"
Video = require "Video"

-- folders
love.filesystem.setIdentity("loop-slider")

-- video
love.filesystem.createDirectory("videos")
folder = love.filesystem.getSaveDirectory() .. "/videos"
files = love.filesystem.getDirectoryItems("videos")

if #files < 1 then
    love.system.openURL("file://".. folder)
    print("Need at least one video file.")
end


-- make and clear the raw folder
love.filesystem.createDirectory("raw") 
raw_folder = love.filesystem.getSaveDirectory() .. "/raw"
raw_files = love.filesystem.getDirectoryItems("raw")
for i = 1, #raw_files do
    love.filesystem.remove(raw_files[i])
    -- print("deleting " .. i)
end 
-- save folder
love.filesystem.createDirectory("saved") 
save_folder = love.filesystem.getSaveDirectory() .. "/saved"


-- thread
love.threaderror = function (thread, error_string)
    error('love.threaderror: ' .. tostring(thread) .. '\n' .. error_string)
end

ffmpeg_thread = love.thread.newThread("ffmpeg.lua")
ffmpeg_thread:start()
request_channel = love.thread.getChannel ('request_channel')

-- video
video = Video()
image = {}
index = 1

-- screen
love.window.setMode(video.width, video.height + 50, {resizable=false, vsync=false})
screen = {}
screen.w, screen.h = love.graphics.getDimensions()

--- slider
full_video_slider = slider(1, screen.h-50, screen.w-3, 1, true)

-- cmd 
ffmpeg_cmd = {}
ffmpeg_cmd.name = files[1]
ffmpeg_cmd.raw_folder = raw_folder
ffmpeg_cmd.save_folder = save_folder 
ffmpeg_cmd.filename = folder .. "/" .. files[1]
ffmpeg_cmd.start_time = 0
ffmpeg_cmd.duration = 10 
ffmpeg_cmd.width = video.width
ffmpeg_cmd.height = video.height
ffmpeg_cmd.fps = video.fps
ffmpeg_cmd.format = "png"

-- create chunk
function create_image_sequence ()
    -- convert slider to time
    local x = full_video_slider.size / full_video_slider.markers[1].x
    local duration_frames = video.duration * video.fps
    local time = video.duration / x
    time = math.floor(time)
    
    -- truncate time
    if time + 10 > video.duration then 
        time = video.duration-10
    end
    ffmpeg_cmd.start_time = time
    
    request_channel:push(ffmpeg_cmd)
end


function export_gif ()
    print("exporting")
    
    local loop_start_frames = (timeline.loops[2].loop_start / timeline.size) * #image
    local loop_start_seconds = loop_start_frames / video.fps

    local loop_duration_timeline = timeline.loops[2].loop_end - timeline.loops[2].loop_start
    local loop_duration_frames = (loop_duration_timeline / timeline.size) * #image
    local loop_duration_seconds = loop_duration_frames / video.fps

    print("start: " .. loop_start_seconds)
    print("duration: " .. loop_duration_seconds)

    local cmd = ffmpeg_cmd
    cmd.format = "gif"
    cmd.duration = loop_duration_seconds
    cmd.start_time = ffmpeg_cmd.start_time + loop_start_seconds

    request_channel = love.thread.getChannel ('request_channel') 
    request_channel:push(cmd) 
end


function load_images()
    local files = love.filesystem.getDirectoryItems("raw")
    for i=1, #files do
        local new_image = love.graphics.newImage("raw/"..files[i])
        image[i] = new_image
        print("loaded " .. files[i]) 
    end 
end



-- input
function love.keypressed(key)
    if key == "o" then
        love.system.openURL("file://".. folder)
    elseif key == " " then
        if timeline then
            timeline.paused = not timeline.paused
        end
    elseif key == "e" then
        if timeline and #timeline.loops==2 then
            export_gif() 
            -- love.system.openURL("file://".. save_folder) 
        end
    elseif key == "r" then
        reload_beginning()
    elseif key == "return" then
        if full_video_slider.markers[1] then
            create_image_sequence()
            full_video_slider.active = false 
            timeline = slider(1, screen.h-50, screen.w-3, 2, false, video.fps) 
            love.timer.sleep(3)
            load_images() 
        end 
    end
end

function love.mousepressed(x, y, button)
    full_video_slider:mousepressed(x, y, button)
    if timeline then
        timeline:mousepressed(x, y, button)
    end 
end


function love.update(dt)
    
    if timeline then
        timeline:update(dt*video.fps*0.1)
        if timeline.playing then
            local adjusted_index = timeline.playback_index / timeline.size
            index = math.floor(adjusted_index * #image)
        elseif timeline.mouse_index then
            local adjusted_index = timeline.mouse_index / timeline.size
            index = math.floor(adjusted_index * #image)
        end 
        
        help.clamp(1, index, #image)
    end
    
    full_video_slider:update(dt) 

end

function love.draw() 
    -- full_video_slider 
    full_video_slider:draw()
    -- draw the time indicator
    if full_video_slider.active then
        local time = ""
        Color.set("white")
        local duration_frames = video.duration * video.fps
        
        if full_video_slider.mouse_index then
            local x = full_video_slider.size / full_video_slider.mouse_index 
            time = video.duration / x
            time = math.floor(time)
        elseif full_video_slider.markers[1] then
            local x = full_video_slider.size / full_video_slider.markers[1].x 
            time = video.duration / x
            time = math.floor(time)
        end
        love.graphics.printf(time, 0, screen.h-35, screen.w, "center") 
    end 
    
    -- timeline
    if timeline then
        timeline:draw()
    end
    
    Color.set ("white")
    if image[index] then
        love.graphics.draw(image[index], 0, 0)
    end
    
    
end
