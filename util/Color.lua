-- Color ------------------------------------

local class = require 'util.class'
local help = require 'util.help'
local Color = class('Color')

-- init ------------------------------------------

function Color:_init(color)
    if type(color) == 'string' then
        color = Color.from_name(color)
    end

    self.r = color and color.r or 0
    self.g = color and color.g or 0
    self.b = color and color.b or 0
    self.a = color and color.a or 1
end

-- named constants -------------------------------

Color.CLEAR   = Color({a = 0})
Color.BLACK   = Color({})
Color.WHITE   = Color({r = 1, g = 1, b =1})
Color.GRAY    = Color({r = 0.5, g = 0.5, b=0.5})
Color.RED     = Color({r = 1})
Color.GREEN   = Color({g = 1})
Color.BLUE    = Color({b = 1})
Color.MAGENTA = Color({r = 1, b = 1})
Color.YELLOW  = Color({r = 1, g = 1})
Color.ORANGE  = Color({r = 1, g = 0.5})
Color.CYAN    = Color({g = 1, b = 1})
Color.BROWN    = Color({r = 0.45, g = 0.3, b=0.1})

-- formats ---------------------------------------

function Color.from_name(name)
    if     name == 'clear'      then return Color.CLEAR
    elseif name == 'black'      then return Color.BLACK
    elseif name == 'white'      then return Color.WHITE
    elseif name == 'gray'       then return Color.GRAY 
    elseif name == 'red'        then return Color.RED
    elseif name == 'green'      then return Color.GREEN
    elseif name == 'blue'       then return Color.BLUE
    elseif name == 'yellow'     then return Color.YELLOW
    elseif name == 'magenta'    then return Color.MAGENTA
    elseif name == 'orange'     then return Color.ORANGE
    elseif name == 'cyan'       then return Color.CYAN
    elseif name == 'brown'       then return Color.BROWN 

    else
        error('cannot create color from name: ' .. name)
    end
end

function Color.from_hex(hex, alpha)
    hex = hex:gsub('#','')
    return Color {r = tonumber('0x'..hex:sub(1,2)) / 255,
            g = tonumber('0x'..hex:sub(3,4)) / 255,
            b = tonumber('0x'..hex:sub(5,6)) / 255,
            a = alpha or 1
        }
end

function Color:hex()
    local r = help.num_to_hex(self.r * 255)
    local g = help.num_to_hex(self.g * 255)
    local b = help.num_to_hex(self.b * 255)

    -- add preceding zeros to make sure each value has 2 digits
    if #r == 1 then r = 0 .. r end
    if #g == 1 then g = 0 .. g end
    if #b == 1 then b = 0 .. b end

    return '#' .. r .. g .. b, self.a
end

function Color.from_bytes(r,g,b,a)
    return Color {
        r = r / 255,
        g = g / 255,
        b = b / 255,
        a = a / 255
    }
end

function Color:bytes()
    return self.r * 255, self.g * 255, self.b * 255, self.a * 255
end

-- get & set -------------------------------------

function Color.get()
    return Color.from_bytes(love.graphics.getColor())
end

function Color.set(color)
    love.graphics.setColor(Color(color):bytes())
end

function Color.get_background()
    return Color.from_bytes(love.graphics.getBackgroundColor())
end

function Color.set_background(color)
    love.graphics.setBackgroundColor(Color(color):bytes())
end

-- HSL -------------------------------------------

-- inspirations:
-- https://github.com/EmmanuelOga/columns/blob/master/utils/color.lua
-- http://www.niwa.nu/2013/05/math-behind-colorspace-conversions-rgb-hsl

function Color:hsl()
    local h, s, l

    -- find min / max rgb values
    local min = math.min(self.r, self.g, self.b)
    local max = math.max(self.r, self.g, self.b)

    -- calculate luminance by adding the max and min valued and divide by 2
    l = (min + max) / 2

    -- if the min and max are the same then there is no saturation or hue (achromatic)
    if min == max then
        h, s = 0, 0
    else
        local d = max - min

        -- if luminance is greater than 0.5 then saturation = (max - min) / (2 - max - min)
        if l > 0.5 then
            s = d / (2 - max - min)
        else
            -- otherwise saturation = (max - min) / (2 - max - min)
            s = d / (max + min)
        end

        if max == self.r then -- red is max
            h = (self.g - self.b) / d

            if self.g < self.b then
                h = h + 6
            end

        elseif max == self.g then -- green is max
            h = (self.b - self.r) / d + 2
        elseif max == self.b then -- blue is max
            h = (self.r - self.g) / d + 4
        end

        h = h / 6
    end

    return h, s, l, self.a
end

function Color.from_hsl(h, s, l, a)
  local color = Color()

  if s == 0 then
    color.r, color.g, color.b = l, l, l -- achromatic
  else
    local function hue2rgb(p, q, t)
      if t < 0   then t = t + 1 end
      if t > 1   then t = t - 1 end
      if t < 1/6 then return p + (q - p) * 6 * t end
      if t < 1/2 then return q end
      if t < 2/3 then return p + (q - p) * (2/3 - t) * 6 end
      return p
    end

    local q
    if l < 0.5 then q = l * (1 + s) else q = l + s - l * s end
    local p = 2 * l - q

    color.r = hue2rgb(p, q, h + 1/3)
    color.g = hue2rgb(p, q, h)
    color.b = hue2rgb(p, q, h - 1/3)
  end

  color.a = a or 1

  return color
end

function Color:get_h()
    local h, s, l = self:hsl()
    return h
end

function Color:get_s()
    local h, s, l = self:hsl()
    return s
end

function Color:get_l()
    local h, s, l = self:hsl()
    return l
end

function Color:set_h(value)
    local h, s, l = self:hsl()
    h = help.clamp(0, value, 1)
    local hsl_color = Color.from_hsl(h,s,l)

    self.r = hsl_color.r
    self.g = hsl_color.g
    self.b = hsl_color.b
end

function Color:set_s(value)
    local h, s, l = self:hsl()
    s = help.clamp(0, value, 1)
    local hsl_color = Color.from_hsl(h,s,l)

    self.r = hsl_color.r
    self.g = hsl_color.g
    self.b = hsl_color.b
end

function Color:set_l(value)
    local h, s, l = self:hsl()
    l = help.clamp(0, value, 1)
    local hsl_color = Color.from_hsl(h,s,l)

    self.r = hsl_color.r
    self.g = hsl_color.g
    self.b = hsl_color.b
end

-- print -----------------------------------------

function Color:__tostring()
    return  '{r = '  .. (self.r or 0) ..
            ', g = ' .. (self.g or 0) ..
            ', b = ' .. (self.b or 0) ..
            ', a = ' .. (self.a or 0) .. '}'
end

return Color
