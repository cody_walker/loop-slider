local slider_draw = {}

-- draw functions 
local line_width_bold = 1.5

local function slider_rect(slider)
    local rect = {}
    rect.x = slider.position.x
    rect.y = slider.position.y
    rect.w = slider.size
    rect.h = slider.h
    rect.color = "white"
    rect.mode = "line" 
    return rect
end

function slider_draw.background(slider)
    love.graphics.setLineWidth(line_width_bold)
    graphics.draw_rectangle(slider_rect(slider)) 
    love.graphics.setLineWidth(1) 
end

function slider_draw.loop_regions(slider)
    local begin_at = 1
    if #slider.loops > 1 then begin_at = 2 end
    for i=begin_at, #slider.loops do
        love.graphics.setLineWidth(line_width_bold)
        local rect = slider_rect(slider)
        rect.x = slider.position.x + slider.loops[i].loop_start 
        rect.w = (slider.position.x + slider.loops[i].loop_end) - rect.x
        rect.color = "gray"
        rect.mode = "fill"
        graphics.draw_rectangle(rect)
        rect.color = "white"
        rect.mode = "line"
        graphics.draw_rectangle(rect) 
        love.graphics.setLineWidth(1) 
    end
end

function slider_draw.playback_index(slider)
    local index_position = slider.position.x + slider.playback_index
    -- Color.set("white")
    -- love.graphics.line(index_position, slider.position.y, index_position, slider.position.y+slider.h)
    local playback_color = Color("black")
    -- fade out if not playing
    if not slider.playing then playback_color.a = 0.5 end 
    Color.set(playback_color) 
    love.graphics.line(index_position, slider.position.y+line_width_bold, index_position, slider.position.y + slider.h - line_width_bold)
    
end

function slider_draw.mouse_index(slider)
    -- local scale
    local index_position = slider.position.x + slider.mouse_index
    local faded_white = Color("white")
    faded_white.a = 0.5
    Color.set(faded_white)
    love.graphics.line(index_position, slider.position.y, index_position, slider.position.y+slider.h)
end

function slider_draw.marker(slider, index)
    local marker = slider.markers[index]
    local x_position = slider.position.x + (marker.x)
    
    
    local faded_white = Color("white")
    faded_white.a = 1
    
    for i=1, #slider.hitbox_markers do
        if slider.hitbox_markers[i] == index then
            faded_white.a = 0.8
        end 
    end
    
        if slider.selected == index then
            faded_white.a = 1
        end
Color.set(faded_white)

love.graphics.line(x_position, slider.position.y, x_position, slider.position.y+slider.h) 
    
-- draw hitbox
-- local hitbox = {
--     x = slider.markers[index].hitbox_top_left.x,
--     y = slider.markers[index].hitbox_top_left.y,
--     w = slider.markers[index].hitbox_bottom_right.x - slider.markers[index].hitbox_top_left.x, 
--     h = slider.h,
--     color = "red",
--     mode = "line" 
-- }
-- graphics.draw_rectangle(hitbox)
end

return slider_draw

